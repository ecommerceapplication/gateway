package com.ecommerceApplication.gateway;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FailBackMethodController {
    @GetMapping("/customerservicefailback")
    public String customerServiceFailBackMethod()
    {
        return "Customer service is longer than expected"+
                " Please try again";
    }
    @GetMapping("/orderedservicefailback")
    public String orderedServiceFailBackMethod()
    {
        return "Ordered service is longer than expected"+
                " Please try again";
    }


}
